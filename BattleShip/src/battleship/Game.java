/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Game {

    private HumanPlayer[] players;

    public Game() {
        String nom1 = JOptionPane.showInputDialog("Usuario: ");
        if (nom1 != null && nom1.contains("@utn.ac.cr")) {
            JOptionPane.showMessageDialog(null, "Usuario agregado exitosamente! ");
        } else {
            JOptionPane.showMessageDialog(null, "Usuario no permitido! ");
        }
        String nom2 = JOptionPane.showInputDialog("Usuario: ");
        if (nom2 != null && nom2.contains("@utn.ac.cr")) {
            JOptionPane.showMessageDialog(null, "Usuario agregado exitosamente! ");
        } else {
            JOptionPane.showMessageDialog(null, "Usuario no permitido! ");
        }

        this.players = new HumanPlayer[]{
            new HumanPlayer(1, nom1),
            new HumanPlayer(2, nom2)
        };
    }

    public void start() {
        int i = 0;
        int j = 1;
        int len = players.length;
        HumanPlayer player = null;

        this.players[i].placeShips();
        this.players[j].placeShips();

        while (players[0].getTotalLivesLeft() > 0
                && players[1].getTotalLivesLeft() > 0) {

            players[i++ % len].fireAt(players[j++ % len]);
            player = (players[0].getTotalLivesLeft() < players[1].getTotalLivesLeft())
                    ? players[1]
                    : players[0];
        }

        System.out.println("Felicidades jugador "+ player.getUser()+" , has ganado!");
    }
}
