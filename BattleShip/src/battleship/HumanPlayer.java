/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import java.awt.Point;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class HumanPlayer implements IPlayer {

    private int totalLivesLeft = 10;

    private int id;
    private String user;
    private Board board;
    private Scanner scanner;

    public HumanPlayer(int id, String user) {
        this.id = id;
        this.user = user;
        this.board = new Board();
        this.scanner = new Scanner(System.in);
    }

    public int getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Board getBoard() {
        return board;
    }

    @Override
    public void placeShips() {
        System.out.println("%n======== Jugador " + user + "- Es hora de colocar tus barcos ========%n");
        board.placeShipsOnBoard();
    }

    @Override
    public void fireAt(IPlayer opponent) {
        board.printBoard();
        System.out.println("%n Muy bien Jugador " + user + " - Ingresa las coordenadas para tu ataque: ");

        boolean isPointValid = false;
        while (!isPointValid) {
            try {
                Point point = new Point(scanner.nextInt(), scanner.nextInt());
                int x = (int) point.getX() - 1;
                int y = (int) point.getY() - 1;

                Result result = ((HumanPlayer) opponent)
                        .getBoard()
                        .getField(x, y)
                        .shootAt();

                if (result == Result.PARTIAL_HIT || result == Result.DESTROYED) {
                    totalLivesLeft--;
                }

                isPointValid = true;
            } catch (IllegalArgumentException e) {
                System.out.printf(e.getMessage());
            }
        }
    }

    @Override
    public int getTotalLivesLeft() {
        return totalLivesLeft;
    }

}
